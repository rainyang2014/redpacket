<?php
/**
 * test git ......
 * 超级现场签到红包模块微站定义
 *
 * @author 芸众
 * @url http://bbs.we7.cc/
 */
defined('IN_IA') or exit('Access Denied');
include "function.php";


class RedpacketModuleSite extends WeModuleSite {



	public function doWebActivity() {
		global $_W;
		include IA_ROOT . "/framework/library/qrcode/phpqrcode.php";

		//这个操作被定义用来呈现 管理中心导航菜单
		$activitys = pdo_fetchall("select * from ims_redpacket_activity where deleted_at is null");
		foreach($activitys as &$item) {
			$url = murl('entry', ['do' => 'activity', 'm' => 'redpacket', 'aid' => $item['id']], true, 1);

			QRcode::png($url,
				IA_ROOT . '/addons/redpacket/data/'.$item['id'].'.png', 0, 3, 1);
			$qrcode = '../addons/redpacket/data/'.$item['id'].'.png';
			$item['qrcode'] = $qrcode;
		}
		//print_r($activitys);exit;
		include $this->template('activity');
	}

	public function doWebAddactivity() {
		global $_W, $_GPC;
		//print_r($this->modulename);exit;
		if ($_POST) {
			$activity = $_GPC['activity'];
			$activity['backup_image'] = $_GPC['backup_image'];
			$activity['starttime'] = strtotime($_GPC['time1']['start']);
			$activity['endtime'] = strtotime($_GPC['time1']['end']);
			//print_r($activity);exit;
			$res = pdo_insert('redpacket_activity', $activity);
			if ($res) {
				$rule['uniacid'] = $_W['uniacid'];
				$rule['name'] = $activity['activity_name'];
				$rule['module'] = $this->modulename;
				$rule['status'] = 1;
				$result = pdo_insert('rule', $rule);
				if ($result) {
					$rule_id = pdo_fetchcolumn("SELECT id FROM ".tablename('rule')." order by id desc LIMIT 1");
					$rulekeyword['rid'] = $rule_id;
					$rulekeyword['uniacid'] = $_W['uniacid'];
					$rulekeyword['content'] = $activity['keyword'];
					$rulekeyword['module'] = $this->modulename;
					$rulekeyword['type'] = 1;
					$rulekeyword['status'] = 1;
					$result = pdo_insert('rule_keyword', $rulekeyword);
					if ($result) {
						message('保存成功', $this->createWebUrl('activity'));
					}
				}
			}

		}
		include $this->template('add_activity');
	}

	public function doWebRecord() {
		include IA_ROOT . "/framework/library/qrcode/phpqrcode.php";
		QRcode::png('测试二维码', false, 0, 18, 1); // creates file

		//这个操作被定义用来呈现 管理中心导航菜单
	}

	
	public function doWebSetting() {

	}

	public function getActivity($id) {
		return pdo_get('redpacket_activity', array('id' => $id));
	}

	//报名表
	public function getFans($openid, $aid) {
		return pdo_get('redpacket_fans', array('openid' => $openid, 'activity_id' => $aid));
	}

	private function delEmpty($v)
	{
		if ($v=="")   //当数组中存在空值和php值时，换回false，也就是去掉该数组中的空值和php值
		{
			return false;
		}
		return true;
	}

	public function doMobileIndex() {
		global $_W, $_GPC;
		$aid = $_GPC['aid'];
		$fans = pdo_getall('redpacket_fans', array('activity_id' => $aid));
		$activity = pdo_get('redpacket_activity', array('id' => $aid));
		include $this->template('index');
	}

	public function doMobileActivity() {
		global $_W, $_GPC;
		load()->app('tpl');

		$aid = intval($_GPC['aid']);
		if (!$aid || empty($aid)) {
			message('参数错误');
		}
		$fans = getUserInfo();
		//$fans['openid'] = 'sdfsdfsdfs';

		$fansa = $this->getFans($fans['openid'], $aid);
		if (!empty($fansa)) {
			message('您已经报过名了', $this->createMobileUrl('index', ['aid' => $aid]));
		}

		if ($_POST) {

			$postFans = $_GPC['fans'];

			if (empty($postFans['openid'])) {
				message('openid不能为空', $this->createMobileUrl('activity'), 'info');

			}

			if (empty($postFans['activity_id'])) {
				message('活动不能为空', $this->createMobileUrl('activity', ['aid' => $aid]), 'info');
			}


			$fansa = $this->getFans($postFans['openid'], $aid);
			if (!empty($fansa)) {
				message('您已经报过名了', $this->createMobileUrl('index', ['aid' => $aid]), 'info');
			}

			$postFans['created_at'] = time();
			$res = pdo_insert('redpacket_fans', $postFans);
			if ($res) {
				message('报名成功','', 'success');
			}
		}

		//print_r($fans);exit;
		$activity = $this->getActivity($aid);
		//echo $_W['openid'];
		include $this->template('activity');
	}

	public function sendNotice() {
		/*
		$wxAccount = WeAccount::create();
		$data = [
			'first' => [
				'value' => '商品购买成功通知',
				'color' => '#173177'
			],
			'nickname' => [
				'value' => '张三',
				'color' => '#173177'
			],
			'product' => [
				'value' => '苹果6',
				'color' => '#173177'
			]
		];

		$wxAccount->sendTplNotice($_W['openid'], 'ssB-HQcCoHeBrYS-dy0eFiP-IcURlWkVEdIeqPLyBIM', $data);
		*/
	}
}
