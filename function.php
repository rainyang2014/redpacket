<?php
/**
 * Created by PhpStorm.
 * User: Rui
 * Date: 2017/5/5
 * Time: 16:12
 */

function getUserInfo() {
    global $_W;
    $account = WeAccount::create($_W['account']);
    //print_r($account);exit;
    //getOauthInfo 方法是获取openid及access_token
    $oauthinfo = $account->getOauthInfo();
    $openid = $oauthinfo['openid'];

    $fans = $account->fansQueryInfo($openid);
    $fans['openid'] = $openid;
    return $fans;
}