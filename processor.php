<?php
/**
 * 超级现场签到红包模块处理程序
 *
 * @author 芸众
 * @url http://bbs.we7.cc/
 */
defined('IN_IA') or exit('Access Denied');

class RedpacketModuleProcessor extends WeModuleProcessor {
	public function respond() {
		//载入日志函数
		load()->func('logging');
		logging_run($this->message);

		$keyword = $this->message['content'];
		$activity = $this->getActivityByKeyword($keyword);
		if (!empty($activity)) {
			return $this->respNews(array(
				'Title' => $activity['activity_name'],
				'Description' => $activity['content'],
				'PicUrl' => tomedia($activity['backup_image']),
				'Url' => $this->createMobileUrl('activity', array('aid' => $activity['id'])),
			));
		}
		
	}
	
	public function getActivityByKeyword($keyword) {
		return pdo_get('redpacket_activity', array('keyword' => $keyword));
	}
}